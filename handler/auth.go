package jwt

import (
	"Gocode/config"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type Response struct {
	StatusCode int32  `json:"status_code"`
	StatusMsg  string `json:"status_msg,omitempty"`
}

type Claims struct {
	Username string `json:"username"`
	Id       int64  `json:"id"`
	jwt.StandardClaims
}

// Auth 鉴权中间件
// 若用户携带的token正确,解析token,将userId放入上下文context中并放行;否则,返回错误信息
func Auth() gin.HandlerFunc {
	return func(context *gin.Context) {
		//auth := context.Request.Header.Get("Authorization")
		auth := context.Query("token")
		// log.Println(auth)
		if len(auth) == 0 {
			context.Abort()
			context.JSON(http.StatusUnauthorized, Response{
				StatusCode: -1,
				StatusMsg:  "Unauthorized",
			})
			return
		}
		token, err := parseToken(auth)
		if err != nil {
			context.Abort()
			context.JSON(http.StatusUnauthorized, Response{
				StatusCode: -1,
				StatusMsg:  "Token Error",
			})
			return
		} else {
			println("token 正确")
		}
		// log.Printf("%#v", token)
		context.Set("userId", token.Id)
		context.Set("userName", token.Username)
		context.Next()
	}
}

// Auth 鉴权中间件
// 若用户携带的token正确,解析token,将userId放入上下文context中并放行;否则,返回错误信息
// 这里的区别是token的参数在post里面
func AuthFromBody() gin.HandlerFunc {
	return func(context *gin.Context) {
		auth := context.Request.PostFormValue("token")
		// log.Printf("%v \n", auth)
		if len(auth) == 0 {
			context.Abort()
			context.JSON(http.StatusUnauthorized, Response{
				StatusCode: -1,
				StatusMsg:  "Unauthorized",
			})
			return
		}
		token, err := parseToken(auth)
		if err != nil {
			context.Abort()
			context.JSON(http.StatusUnauthorized, Response{
				StatusCode: -1,
				StatusMsg:  "Token Error",
			})
			return
		} else {
			println("token 正确")
		}
		// log.Printf("%#v", token)
		context.Set("userId", token.Id)
		context.Set("userName", token.Username)
		context.Next()
	}
}

// Auth 鉴权中间件
// 未登录时，如果没有token，放入默认值0，若有token，放入解析得到的id
func AuthWithoutLogin() gin.HandlerFunc {
	return func(context *gin.Context) {
		auth := context.Query("token")
		// log.Println(auth)
		if len(auth) == 0 {
			context.Set("userId", 0)
			context.Set("userName", "NotLogin")
			context.Next()
		}
		token, err := parseToken(auth)
		if err != nil {
			context.Abort()
			context.JSON(http.StatusUnauthorized, Response{
				StatusCode: -1,
				StatusMsg:  "Token Error",
			})
			return
		} else {
			println("token 正确")
		}
		// log.Printf("%#v", token)
		context.Set("userId", token.Id)
		context.Set("userName", token.Username)
		context.Next()
	}
}

// parseToken 解析token
func parseToken(token string) (*Claims, error) {
	jwtToken, err := jwt.ParseWithClaims(token, &Claims{}, func(token *jwt.Token) (i interface{}, e error) {
		return []byte(config.JwtSecret), nil
	})

	if err == nil && jwtToken != nil {
		if claim, ok := jwtToken.Claims.(*Claims); ok && jwtToken.Valid {
			// log.Printf("%v", claim)
			return claim, nil
		}
	}
	return nil, err
}
