package service

import (
	"Gocode/common"
	"Gocode/dao"
	"bytes"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/disintegration/imaging"
	ffmpeg "github.com/u2takey/ffmpeg-go"
	// //加密token用
	// "strconv"
	// "time"
	// "github.com/dgrijalva/jwt-go"
)

// VideoServiceImpl 结构体
type VideoServiceImpl struct {
	// FollowService
	// LikeService
	UserServiceImpl
}

const baseUrl = "http://localhost:8080/static/"

// GetVideoByTime 根据latestTime获得TableVideos
func (usi *UserServiceImpl) GetVideoByTime(latestTime string) []dao.TableVideo {
	tableVideos, err := dao.GetTableVideoByTime(latestTime)
	if err != nil {
		log.Println("Err:", err.Error())
		log.Println("User Not Found")
		return tableVideos
	}
	// log.Println("Query Videos Success")
	return tableVideos
}

func (usi *UserServiceImpl) ConvertTableVideoToVideoDto(tableVideos []dao.TableVideo) []common.VideoDto {
	userService := UserServiceImpl{}
	var videoDtos []common.VideoDto
	for _, tableVideo := range tableVideos {
		userDto, err := userService.GetUserById(tableVideo.PusherId)
		if err != nil {
			return videoDtos
		}
		videoDtos = append(videoDtos, common.VideoDto{
			Author:        userDto,
			CommentCount:  tableVideo.CommentCount,
			CoverURL:      tableVideo.CoverUrl,
			FavoriteCount: tableVideo.FavoriteCount,
			ID:            tableVideo.Id,
			IsFavorite:    false, // TODO，此处需要完善【需要根据token来获得用户是否点赞该视频】
			PlayURL:       tableVideo.PlayUrl,
			Title:         tableVideo.Title,
		})
	}
	return videoDtos
}
func (usi *UserServiceImpl) GetVideoByUserId(userId string) []dao.TableVideo {
	tableVideos, err := dao.GetTableVideoByUserId(userId)
	if err != nil {
		log.Println("Err:", err.Error())
		log.Println("User Not Found")
		return tableVideos
	}
	// log.Println("Query Videos Success")
	return tableVideos
}
func (usi *UserServiceImpl) PublishVideo(userId int64, fileName string, title string) error {

	err := GetSnapshot("./public/"+fileName, "./public/"+fileName+".jpg", 1)
	if err != nil {
		return err
	}
	err = dao.CreatVideo(dao.TableVideo{
		PlayUrl:       baseUrl + fileName,
		CoverUrl:      baseUrl + fileName + ".jpg",
		CommentCount:  0,
		FavoriteCount: 0,
		Title:         title,
		PublishTime:   time.Now(),
		PusherId:      userId,
	})
	return err
}

func GetSnapshot(videoPath, snapshotPath string, frameNum int) error {
	buf := bytes.NewBuffer(nil)
	err := ffmpeg.Input(videoPath).
		Filter("select", ffmpeg.Args{fmt.Sprintf("gte(n,%d)", frameNum)}).
		Output("pipe:", ffmpeg.KwArgs{"vframes": 1, "format": "image2", "vcodec": "mjpeg"}).
		WithOutput(buf, os.Stdout).
		Run()
	if err != nil {
		log.Fatal("生成缩略图失败：", err)
		return err
	}

	img, err := imaging.Decode(buf)
	if err != nil {
		log.Fatal("生成缩略图失败：", err)
		return err
	}

	err = imaging.Save(img, snapshotPath)
	if err != nil {
		log.Fatal("生成缩略图失败：", err)
		return err
	}
	return err
}

// 根据ID获取视频信息
func (usi *UserServiceImpl) GetVideoInfoById(VideoID int64) (*common.VideoDto, error) {
	get, err := dao.GetVideoByVideoId(VideoID)
	if err != nil {
		return nil, err
	} else if get == nil {
		log.Println("GetVideoInfoById 获取为空")
		return nil, nil
	} else {
		return nil, nil //错误返回
	}
}

// 根据ID获取视频信息
func (vsi *VideoServiceImpl) GetVideoInfoByvideoId(VideoID int64) (*dao.TableVideo, error) {
	get, err := dao.GetVideoByVideoId(VideoID) //异常
	if err != nil {
		return nil, err
	} else if get == nil {
		log.Println("GetVideoInfoById 获取为空")
		return nil, nil
	} else {
		return get, nil
	}
}

func (vsi *VideoServiceImpl) GetVideoCountById(userid int64) (int64, error) {
	count, err := dao.GetVideoCountById(userid)
	if err != nil {
		return 0, err
	}
	return count, nil
}
