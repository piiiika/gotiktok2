package service

import (
	"log"
	"testing"
)

//启动测试代码： go test -v .\service

func TestGenerateToken(t *testing.T) {
	token, err := GenerateToken("root", 64)
	log.Printf("%v", token)
	log.Printf("%v", err)
}

func TestParseToken(t *testing.T) {
	token, err := GenerateToken("root", 64)
	log.Printf("%v", err)
	res, err := ParseToken(token)
	log.Printf("%v", err)
	log.Printf("%v", res)
}

func TestParseTokenID(t *testing.T) {
	token, err := GenerateToken("root", 64)
	log.Printf("%v", err)
	res, err := ParseTokenID(token)
	log.Printf("%v", err)
	log.Printf("%v", res)
}

func TestParseTokenUsername(t *testing.T) {
	token, err := GenerateToken("root", 64)
	log.Printf("%v", err)
	res, err := ParseTokenUsername(token)
	log.Printf("%v", err)
	log.Printf("%v", res)
}
