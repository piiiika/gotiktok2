package service

import (
	"Gocode/common"
	"Gocode/dao"
)

func MessageChatService(userId int64, toUserId int64, preMsgTime int64) ([]common.MessageDto, error) {
	var message []common.MessageDto
	message, err := dao.GetMessageByUserIdAndToUserId(userId, toUserId, preMsgTime)
	if err != nil {
		return nil, err
	}
	return message, nil
}

func MessageAction(userId int64, toUserId int64, nowTime int64, content string) error {
	err := dao.InsertMessage(userId, toUserId, nowTime, content)
	return err
}
