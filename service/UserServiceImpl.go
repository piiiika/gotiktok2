package service

import (
	"Gocode/common"
	"Gocode/config"
	"Gocode/dao"
	"fmt"
	"strconv"

	// "fmt"
	"log"

	//密码的加密
	"golang.org/x/crypto/bcrypt"
	// //加密token用
	// "strconv"
	// "time"
	// "github.com/dgrijalva/jwt-go"
)

// userServiceImpl结构体
type UserServiceImpl struct {
	// FollowService
	// LikeService
}

// GetTableUserByUsername 根据username获得TableUser对象
func (usi *UserServiceImpl) GetTableUserByUsername(name string) dao.TableUser {
	tableUser, err := dao.GetTableUserByUsername(name)
	if err != nil {
		log.Println("Err:", err.Error())
		log.Println("User Not Found")
		return tableUser
	}
	log.Println("Query User Success")
	return tableUser
}

// GetUserById 未登录情况下,根据user_id获得User对象
func (usi *UserServiceImpl) GetUserById(id int64) (common.UserDto, error) {
	user := common.UserDto{}

	tableUser, err := dao.GetTableUserById(id)
	if err != nil {
		log.Println("Err:", err.Error())
		log.Println("User Not Found")
		return user, err
	}
	rsi := RelationServiceImpl{}
	vsi := VideoServiceImpl{}
	followCount, err := rsi.GetFollowingCnt(id)
	if err != nil {
		return user, err
	}
	followerCount, err := rsi.GetFollowerCnt(id)
	if err != nil {
		return user, err
	}
	likeCount, err := rsi.FavouriteVideoCount(id)
	if err != nil {
		return user, err
	}

	videoCount, err := vsi.GetVideoCountById(id)
	if err != nil {
		return user, err
	}

	totalFavorited, err := rsi.TotalUserFavourite(id)

	if err != nil {
		return user, err
	}

	//is_follow暂时没有做
	user = common.UserDto{
		Id:              id,
		Name:            tableUser.Username,
		FollowCount:     followCount,
		FollowerCount:   followerCount,
		IsFollow:        false,
		FavoriteCount:   likeCount,
		WorkCount:       videoCount,
		Signature:       config.DefaultSignature,
		Avatar:          config.DefaultAvater,
		BackgroundImage: config.DefaultBackGround,
		TotalFavorited:  strconv.FormatInt(totalFavorited, 10),
	}
	log.Printf("%#v", user)
	return user, nil
}

// GetUserById 已经登录情况下,根据user_id获得User对象 id是要查找的用户id，currentid是你的用户id
func (usi *UserServiceImpl) GetUserByIdWithCurId(id int64, currentId int64) (common.UserDto, error) {
	user := common.UserDto{}

	tableUser, err := dao.GetTableUserById(id)
	if err != nil {
		log.Println("Err:", err.Error())
		log.Println("User Not Found")
		return user, err
	}
	rsi := RelationServiceImpl{}
	vsi := VideoServiceImpl{}
	followCount, err := rsi.GetFollowingCnt(id)
	if err != nil {
		return user, err
	}
	followerCount, err := rsi.GetFollowerCnt(id)
	if err != nil {
		return user, err
	}
	likeCount, err := rsi.TotalUserFavourite(id)
	if err != nil {
		return user, err
	}

	videoCount, err := vsi.GetVideoCountById(id)
	if err != nil {
		return user, err
	}

	totalFavorited, err := rsi.TotalUserFavourite(id)

	if err != nil {
		return user, err
	}

	is_follow, err := rsi.IsFollowing(currentId, id)
	if err != nil {
		return user, err
	}

	user = common.UserDto{
		Id:              id,
		Name:            tableUser.Username,
		FollowCount:     followCount,
		FollowerCount:   followerCount,
		IsFollow:        is_follow,
		FavoriteCount:   likeCount,
		WorkCount:       videoCount,
		Signature:       config.DefaultSignature,
		Avatar:          config.DefaultAvater,
		BackgroundImage: config.DefaultBackGround,
		TotalFavorited:  strconv.FormatInt(totalFavorited, 10),
	}
	// log.Printf("%#v", user)
	return user, nil
}

// InsertTableUser 将tableUser插入表内
func (usi *UserServiceImpl) InsertTableUser(tableUser *dao.TableUser) bool {
	flag := dao.InsertTableUser(tableUser)
	if !flag {
		log.Println("插入失败")
		return false
	}
	return true
}

// 使用加盐哈希的方法加密密码
func EnCoder(password string) string {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		fmt.Println(err)
	}
	encodePWD := string(hash)
	fmt.Println(encodePWD)
	return encodePWD
}

// 检验密码是否正确 1正确 0错误
func CheckPassword(dbPassword string, input string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(dbPassword), []byte(input))
	if err != nil {
		return false
	} else {
		return true
	}
}

// // GenerateToken 根据username生成一个token
// func GenerateToken(username string) string {
// 	u := UserService.GetTableUserByUsername(username)
// 	fmt.Printf("generatetoken: %v\n", u)
// 	token := NewToken(u)
// 	println(token)
// 	return token
// }

// // NewToken 根据信息创建token
// func NewToken(u dao.TableUser) string {
// 	expiresTime := time.Now().Unix() + int64(config.OneDayOfHours)
// 	fmt.Printf("expiresTime: %v\n", expiresTime)
// 	id64 := u.Id
// 	fmt.Printf("id: %v\n", strconv.FormatInt(id64, 10))
// 	claims := jwt.StandardClaims{
// 		Audience:  u.Name,
// 		ExpiresAt: expiresTime,
// 		Id:        strconv.FormatInt(id64, 10),
// 		IssuedAt:  time.Now().Unix(),
// 		Issuer:    "tiktok",
// 		NotBefore: time.Now().Unix(),
// 		Subject:   "token",
// 	}
// 	var jwtSecret = []byte(config.Secret)
// 	tokenClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
// 	if token, err := tokenClaims.SignedString(jwtSecret); err == nil {
// 		token = "Bearer " + token
// 		println("generate token success!\n")
// 		return token
// 	} else {
// 		println("generate token fail\n")
// 		return "fail"
// 	}
// }
