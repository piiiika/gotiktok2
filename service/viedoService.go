package service

import (
	"Gocode/common"
	"Gocode/dao"
)

type VideoService interface {
	// GetVideoByTime 根据latestTime获得TableUser对象集合
	GetVideoByTime(latestTime string) []dao.TableVideo

	// ConvertTableVideoToVideoDto 将得到的TableVideo转化为VideoDto，方便以JSON返回
	ConvertTableVideoToVideoDto(tableVideos []dao.TableVideo) []common.VideoDto

	// GetVideoByUserId 根据userId获得TableUser对象集合
	GetVideoByUserId(userId string) []dao.TableVideo

	PublishVideo(userId int64, fileName string, title string) error

	//根据ID获取视频信息
	GetVideoInfoById(VideoID int64) (dao.TableVideo,error)
}
