package service

import (
	"Gocode/common"
)

type MessageService interface {

	//消息列表
	MessageChatService(userId int64, toUserId int64, preMsgTime int64) ([]common.MessageDto, error)

	//信息发送
	MessageAction(userId int64, toUserId int64, nowTime int64, content string) error
}
