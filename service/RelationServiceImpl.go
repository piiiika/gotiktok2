package service

import (
	"Gocode/common"
	"Gocode/dao"
	"log"
	"strconv"
	"sync"
)

type RelationServiceImpl struct {
	UserServiceImpl
	VideoServiceImpl
}

// 是否关注操作
func (rsi *RelationServiceImpl) ActionById(userId int64, targetId int64, action_type int8) (bool, error) {
	// 查询是否存在过关系
	isfollow, err := rsi.EverFollowing(userId, targetId)
	//处理查询错误
	if err != nil {
		return false, err
	}

	log.Printf("关注操作触发:Isfollow判断为:  %v", isfollow)
	//存在历史关注，修改为已关注或未关注，is_follow = 1 or 2
	if isfollow {
		res, err := dao.UpdateFollowRelation(userId, targetId, action_type)
		if res {
			return true, nil
		} else {
			return false, err
		}
	} else {
		//不存在历史关系，则插入一条新的关注数据
		res, err := dao.InsertFollowRelation(userId, targetId)
		if res {
			return true, nil
		} else {
			return false, err
		}
	}
}

// 给定当前用户和目标用户id，判断是否存在关注关系。
func (rsi *RelationServiceImpl) IsFollowing(userId int64, targetId int64) (bool, error) {
	// 查询
	relation, err := dao.FindRelation(userId, targetId)
	//关系不存在或错误
	if nil != err {
		return false, err
	}
	if nil == relation {
		return false, nil
	}
	// 存在关系
	return true, nil
}

// 查询是否存在历史关系
func (rsi *RelationServiceImpl) EverFollowing(userId int64, targetId int64) (bool, error) {
	relation, err := dao.GetEverFollowing(userId, targetId)
	//关系不存在或错误
	if nil != err {
		return false, err
	}
	if nil == relation {
		return false, nil
	}
	// 存在关系
	return true, nil
}

// 获取关注列表
func (rsi *RelationServiceImpl) GetFollowing(userId int64) ([]common.UserDto, error) {
	// 获取关注对象的id数组。
	ids, err := dao.GetFollowingIds(userId)

	// 查询出错
	if nil != err {
		return nil, err
	}
	// 未拥有关注者
	if nil == ids {
		return nil, nil
	}
	// 根据每个id来查询用户信息。
	following, _ := rsi.getUserById(ids, userId)
	// 返回关注对象列表。
	return following, nil
}

// 当前用户被关注数量
func (rsi *RelationServiceImpl) GetFollowerCnt(userId int64) (int64, error) {
	cnt, err := dao.GetFollowerCnt(userId)
	if err != nil {
		return -1, err
	} else {
		return cnt, nil
	}
}

// 当前用户关注其它用户数量
func (rsi *RelationServiceImpl) GetFollowingCnt(userId int64) (int64, error) {
	cnt, err := dao.GetFollowingCnt(userId)
	if err != nil {
		return 0, err
	} else {
		return cnt, nil
	}
}

// 获取当前用户的粉丝列表
func (rsi *RelationServiceImpl) GetFollowers(userId int64) ([]common.UserDto, error) {
	// 获取当前用户粉丝的id数组。
	ids, err := dao.GetFollowerIds(userId)

	// 查询出错
	if nil != err {
		return nil, err
	}
	// 未拥有关注者
	if nil == ids {
		return nil, nil
	}
	// 根据每个id来查询用户信息。
	follower, _ := rsi.getUserById(ids, userId)
	return follower, nil
}

func (rsi *RelationServiceImpl) GetFriendList(userId int64) ([]common.UserDto, error) {
	ids, err := dao.GetFriendList(userId)

	// 查询出错
	if nil != err {
		return nil, err
	}
	// 未拥有关注者
	if nil == ids {
		return nil, nil
	}
	// 根据每个id来查询用户信息。
	friendList, _ := rsi.getUserById(ids, userId)
	return friendList, nil
}

// 获取相关用户信息
func (rsi *RelationServiceImpl) getUserById(userIds []int64, currentId int64) ([]common.UserDto, error) {
	// 根据每个id来查询用户信息。
	len := len(userIds)
	// if len > 0 {
	// 	len -= 1
	// }
	var wg sync.WaitGroup
	wg.Add(len)
	users := make([]common.UserDto, len)

	for i, j := 0, 0; i < len; j++ {
		if userIds[j] == -1 {
			continue
		}

		go func(i int, idx int64) {
			defer wg.Done()
			users[i], _ = rsi.GetUserByIdWithCurId(idx, currentId)
		}(i, userIds[i])
		i++
	}
	wg.Wait()
	// 返回用户列表。
	return users, nil

}

// 判断用户是否有点赞
func (rsi *RelationServiceImpl) IfFavourite(userId int64, videoId int64) (bool, error) {
	//查询点赞信息
	res, err := dao.GetEverLike(userId, videoId)
	//错误处理返回
	if err != nil {
		return false, err
	} else if res == nil {
		return false, nil
	} else {
		return true, nil
	}

}

// 根据ID获取视频的点赞数量
func (rsi *RelationServiceImpl) FavouriteCount(videoId int64) (int64, error) {
	//查询点赞该视频的用户
	res, err := dao.GetLikeUserIdList(videoId)
	//错误处理返回
	if err != nil {
		return -1, err
	} else if res == nil {
		return 0, nil
	} else {
		cnt := int64(len(res))
		return cnt, nil
	}
}

// 根据userId获取这个用户点赞的视频数量
func (rsi *RelationServiceImpl) FavouriteVideoCount(userId int64) (int64, error) {
	//查询该用户点赞过的视频
	res, err := dao.GetLikeVideoIdList(userId)
	//错误处理返回
	if err != nil {
		return -1, err
	} else if res == nil {
		return 0, nil
	} else {
		cnt := int64(len(res))
		return cnt, nil
	}
}

// 根据userId获取这个用户被点赞数
func (rsi *RelationServiceImpl) TotalUserFavourite(userId int64) (int64, error) {
	//获取当前用户发布的视频
	getUserVideo := rsi.GetVideoByUserId(strconv.Itoa(int(userId)))

	var allcnt int64

	if getUserVideo == nil {
		return 0, nil
	} else {
		//提前开辟空间,存取每个视频的点赞数
		videoLikeCountList := new([]int64)
		//采用协程并发将对应videoId的点赞数添加到集合中去
		i := len(getUserVideo)
		var wg sync.WaitGroup
		wg.Add(i)
		for j := 0; j < i; j++ {
			go rsi.addVideoLikeCount(getUserVideo[j].Id, videoLikeCountList, &wg)
		}
		wg.Wait()
		//遍历累加，求总被点赞数
		for _, count := range *videoLikeCountList {
			allcnt += count
		}
		return allcnt, nil
	}

}

// 根据videoId，分别加入该视频点赞数
func (rsi *RelationServiceImpl) addVideoLikeCount(videoId int64, videoLikeCountList *[]int64, wg *sync.WaitGroup) {
	defer wg.Done()
	//调用FavouriteCount：根据videoId,获取点赞数
	count, err := rsi.FavouriteCount(videoId)
	if err != nil {
		//如果有错误，输出错误信息，并不加入该视频点赞数
		log.Println(err.Error())
		return
	}

	*videoLikeCountList = append(*videoLikeCountList, count)
}

// 更新当前操作行为，1点赞，2取消点赞。
func (rsi *RelationServiceImpl) FavouriteAction(userId int64, videoId int64, actionType int8) (bool, error) {
	//查询是否拥有点赞关系
	res, err := rsi.IfFavourite(userId, videoId)
	if err != nil {
		return false, err
	} else if res {
		//更新点赞操作
		get, renewerr := dao.UpdateLikeAction(userId, videoId, actionType)
		if renewerr != nil {
			return false, renewerr
		}
		if !get {
			return get, nil
		}
		return get, nil
	} else {
		//插入点赞信息
		inerr := dao.InsertLike(userId, videoId)
		if inerr != nil {
			return false, inerr
		} else {
			return true, nil
		}
	}
}

// 获取当前用户的所有点赞视频
func (rsi *RelationServiceImpl) GetFavouriteList(userId int64) ([]common.VideoDto, error) {
	//获取用户点赞视频ID列表
	videoListID, err := dao.GetLikeVideoIdList(userId)

	//存储视频信息
	var VideoList []common.VideoDto

	if err != nil {
		return nil, err
	} else {
		//采用协程并发
		i := len(videoListID)

		// var wg sync.WaitGroup
		// wg.Add(i)
		for j := 0; j < i; j++ {
			rsi.RsiGetVideoInfoByID(videoListID[j], userId, &VideoList) //异常
		}
		// wg.Wait()

		return VideoList, nil
	}

}

// 抽取视频作者用户ID
func (rsi *RelationServiceImpl) GetVideoAuthorID(videoId int64) int64 {

	videotable, err := rsi.GetVideoInfoByvideoId(videoId)
	if err != nil {
		log.Println("抽取ID:查询不到视频作者ID")
		return -1
	}

	return videotable.PusherId
}

// 根据视频ID整合获取视频信息
func (rsi *RelationServiceImpl) RsiGetVideoInfoByID(videoId int64, curr_userid int64, videoList *[]common.VideoDto) {
	// defer wg.Done()
	//获取视频信息
	video, err := rsi.GetVideoInfoByvideoId(videoId) //异常
	if err != nil {
		log.Println(err.Error())
		return
	}
	//获取视频作者信息
	author, err := rsi.GetUserByIdWithCurId(video.PusherId, curr_userid)
	if err != nil {
		log.Println(err.Error())
		return
	}

	var packVideodto common.VideoDto
	packVideodto.Author = author
	packVideodto.ID = video.Id
	packVideodto.PlayURL = video.PlayUrl
	packVideodto.CoverURL = video.CoverUrl
	packVideodto.IsFavorite = true
	packVideodto.CommentCount = video.CommentCount
	packVideodto.FavoriteCount = video.FavoriteCount

	*videoList = append(*videoList, packVideodto)

}
