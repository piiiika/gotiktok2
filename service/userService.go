package service

import "Gocode/dao"

type UserService interface {
	// GetTableUserByUsername 根据username获得TableUser对象
	GetTableUserByUsername(name string) dao.TableUser

	// GetTableUserById 根据user_id获得TableUser对象
	GetTableUserById(id int64) dao.TableUser

	// InsertTableUser 将tableUser插入表内
	InsertTableUser(tableUser *dao.TableUser) bool
}
