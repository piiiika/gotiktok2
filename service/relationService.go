package service

import (
	"Gocode/common"
	"Gocode/dao"
)

type RelationService interface {

	//根据对方ID关注用户
	ActionById(Id string, Action_Type string) (bool, error)
	//用户取消对目标用户的关注
	// DeleteFollowRelation(userId int64, targetId int64) (bool, error)
	//判断是否已关注
	IsFollower(userId int64, targetId int64) (bool, error)
	//当前用户被关注数量
	GetFollowerCnt(userId int64) (int64, error)
	//当前用户关注其它用户数量
	GetFollowingCnt(userId int64) (int64, error)
	//获取当前用户的关注列表
	GetFollowing(userId int64) ([]common.UserDto, error)
	//获取当前用户的粉丝列表
	GetFollowers(userId int64) ([]common.UserDto, error)
	//获取好友列表
	GetFriendList(userId int64) ([]common.UserDto, error)

	//IsFavorite 根据当前视频id判断是否点赞了该视频。
	IsFavourite(userId int64, videoId int64) (bool, error)
	//FavouriteCount 根据当前视频id获取当前视频点赞数量。
	FavouriteCount(videoId int64) (int64, error)
	//TotalFavourite 根据userId获取这个用户总共被点赞数量
	TotalUserFavourite(userId int64) (int64, error)
	//FavouriteVideoCount 根据userId获取这个用户点赞视频数量
	FavouriteVideoCount(userId int64) (int64, error)

	//更新当前操作行为，1点赞，2取消点赞。
	FavouriteAction(userId int64, videoId int64, actionType int32) (bool, error)
	// GetFavouriteList 获取当前用户的所有点赞视频
	GetFavouriteList(userId int64) ([]dao.TableVideo, error)
}
