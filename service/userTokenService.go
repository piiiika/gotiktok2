package service

import (
	"Gocode/config"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// 指定加密密钥
var jwtSecret = []byte(config.JwtSecret)

// Claim是一些实体（通常指的用户）的状态和额外的元数据
type Claims struct {
	Username string `json:"username"`
	Id       int64  `json:"id"`
	jwt.StandardClaims
}

// 根据用户的用户名产生token
func GenerateToken(username string, id int64) (string, error) {
	//设置token有效时间 24h
	nowTime := time.Now()
	expireTime := nowTime.Add(24 * time.Hour)

	claims := Claims{
		Username: username,
		Id:       id,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expireTime.Unix(),
			Issuer:    "piiiika",
		},
	}

	tokenClaims := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	//该方法内部生成签名字符串，再用于获取完整、已签名的token
	token, err := tokenClaims.SignedString(jwtSecret)
	return token, err
}

// 根据传入的token值获取到Claims对象信息，（进而获取其中的用户名和密码）
func ParseToken(token string) (*Claims, error) {

	//用于解析鉴权的声明，方法内部主要是具体的解码和校验的过程，最终返回*Token
	tokenClaims, err := jwt.ParseWithClaims(token, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return jwtSecret, nil
	})

	if tokenClaims != nil {
		// 从tokenClaims中获取到Claims对象，并使用断言，将该对象转换为我们自己定义的Claims
		// 要传入指针，项目中结构体都是用指针传递，节省空间。
		if claims, ok := tokenClaims.Claims.(*Claims); ok && tokenClaims.Valid {
			return claims, nil
		}
	}
	return nil, err
}

// 从token中获取用户的用户名
func ParseTokenUsername(token string) (string, error) {
	tokenClaims, err := ParseToken(token)
	if err != nil {
		return "", err
	}
	return tokenClaims.Username, nil
}

// 从token中获取用户的id
func ParseTokenID(token string) (int64, error) {
	tokenClaims, err := ParseToken(token)
	if err != nil {
		return 0, err
	}
	return tokenClaims.Id, nil
}
