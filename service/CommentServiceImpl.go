package service

import (
	"Gocode/common"
	"Gocode/dao"
	"log"
)

type CommentServiceImpl struct {
	UserServiceImpl
}

// 使用video id 查询Comment数量
func (csi CommentServiceImpl) CountFromVideoId(videoId int64) (int64, error) {
	//数据库查询
	res, err := dao.CommentCount(videoId)
	if err != nil {
		log.Println("评论功能: 数据库查询Comment数量失败")
		return -1, err
	} else {
		return res, nil
	}
}

// 发布评论
func (csi CommentServiceImpl) SendComment(comment dao.TableComment) (common.Comment, error) {

	//插入评论
	res, err := dao.InsertComment(comment)
	if err != nil {
		log.Println("comment: insert comment fail!!")
		return common.Comment{}, err
	} else {
		commentInfo := csi.PackCommentResponse(res)
		return commentInfo, nil
	}
}

// 删除评论
func (csi CommentServiceImpl) DeleteComment(cId int64) error {
	//查询是否存在此评论
	isexist, err := dao.GetCommentExist(cId)
	if !isexist {
		return err
	} else {
		//删除评论
		res := dao.DeleteComment(cId)
		if res != nil {
			return res //删除操作错误
		} else {
			return nil
		}
	}

}

// 获取评论列表
func (csi CommentServiceImpl) GetCommentList(videoId int64) ([]common.Comment, error) {
	//获取评论列表
	c_list, err := dao.GetCommentList(videoId)
	if err != nil {
		log.Println("comment : get comment list fail!!")
		return nil, err
	}
	//当前评论0条
	if c_list == nil {
		return nil, nil
	}
	//封装评论信息
	var commentlist []common.Comment
	for _, comment := range c_list {
		commentdto := csi.PackCommentResponse(comment)
		commentlist = append(commentlist, commentdto)
	}
	return commentlist, nil
}

// 封装Comment响应
func (csi CommentServiceImpl) PackCommentResponse(tablecomment dao.TableComment) common.Comment {
	//获取评论用户信息
	userid := tablecomment.UserId
	userInfo, _ := csi.GetUserById(userid)
	var commentInfo common.Comment

	commentInfo.ID = tablecomment.Id
	commentInfo.User = userInfo
	commentInfo.Content = tablecomment.CommentText
	commentInfo.CreateDate = tablecomment.CreateDate.String()

	return commentInfo
}
