package service

import (
	"Gocode/common"
	"Gocode/dao"
)

type CommentService interface {
	//获取评论数
	CountFromVideoId(videoId int64) (int64, error)
	//发布评论
	SendComment(comment dao.TableComment) (common.Comment, error)
	//删除评论
	DeleteComment(cId int64) error
	//获取评论列表
	GetCommentList(videoId int64) ([]common.Comment, error)
	//数据封装
	PackCommentResponse(tablecomment dao.TableComment) common.Comment
}