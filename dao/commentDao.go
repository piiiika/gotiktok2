package dao

import (
	"errors"
	"log"
	"time"
)

type TableComment struct {
	Id          int64     //评论id
	UserId      int64     //评论用户id
	VideoId     int64     //视频id
	CommentText string    //评论内容
	CreateDate  time.Time //评论发布的日期
	IsDelete    int8      //软删除
}

// 修改表名映射
func (tableComment TableComment) TableName() string {
	return "comment"
}

// 使用videoid查询Comment数量
func CommentCount(videoId int64) (int64, error) {
	var count int64
	//数据库中查询评论数量
	err := Db.Model(TableComment{}).Where(map[string]interface{}{"video_id": videoId, "is_delete": 1}).Count(&count).Error
	if err != nil {
		log.Println("CommentDao-Count: return count failed")
		return -1, errors.New("find comments count failed")
	}
	return count, nil
}

// 根据视频id获取评论id列表
func GetCommentIdList(videoId int64) ([]int64, error) {
	var commentIdList []int64
	err := Db.Model(TableComment{}).Select("id").Where("video_id = ?", videoId).Find(&commentIdList).Error
	if err != nil {
		log.Println("CommentIdList err :", err)
		return nil, err
	}
	return commentIdList, nil
}

// 发表评论
func InsertComment(comment TableComment) (TableComment, error) {
	//数据库中插入一条评论信息
	comment.IsDelete = 1
	err := Db.Model(TableComment{}).Create(&comment).Error

	cnt, _ := CommentCount(comment.VideoId)
	log.Printf("InsertComment,vedioID : %d, count : %d", comment.VideoId, cnt)
	errv :=Db.Model(TableVideo{}).Where("id = ?",comment.VideoId).Update("comment_count",cnt).Error
	if err != nil {
		log.Println("CommentDao-InsertComment: return create comment failed") //函数返回提示错误信息
		return TableComment{}, err
	}
	if errv != nil{
		log.Println(err) //函数返回提示错误信息
		return TableComment{}, errors.New("update comment-count failed")
	}
	// t := time.Now().Format("2006-01-02")
	// log.Println(t)
	// comment.CreateDate,_ = time.Parse("01-02",t)
	return comment, nil
}

// 删除评论
func DeleteComment(cid int64) error {
	//先查询是否有此评论
	exist, eerr := GetCommentExist(cid)
	if !exist {
		return eerr
	}

	//数据库中删除评论-更新评论状态为 2
	err := Db.Model(TableComment{}).Where("id = ?", cid).Update("is_delete", 2).Error
	res := GetCommentInfoById(cid)
	cnt, _ := CommentCount(res.VideoId)
	log.Println(res)
	log.Println(cnt)
	Db.Model(TableVideo{}).Where("id = ?", res.VideoId).Update("comment_count", cnt)
	if err != nil {
		log.Println("CommentDao-DeleteComment: return del comment failed") //函数返回提示错误信息
		return errors.New("del comment failed")
	}
	log.Println("CommentDao-DeleteComment: return success") //函数执行成功，返回正确信息
	return nil
}

// 根据评论ID查询是否存在
func GetCommentExist(cid int64) (bool, error) {
	var comment TableComment
	//查询数据库
	res := Db.Model(TableComment{}).Where(map[string]interface{}{"id": cid, "is_delete": 1}).First(&comment)
	if res.RowsAffected == 0 { //查询到此评论数量为0则返回无此评论
		log.Println("CommentDao-DeleteComment: return del comment is not exist") //函数返回提示错误信息
		return false, errors.New("del comment is not exist")
	} else {
		return true, nil
	}
}

// 根据视频id查询评论列表
func GetCommentList(videoId int64) ([]TableComment, error) {
	//数据库中查询评论信息list
	var commentList []TableComment
	result := Db.Model(TableComment{}).
		Where(map[string]interface{}{"video_id": videoId, "is_delete": 1}).
		Order("create_date desc").
		Find(&commentList)
	//若此视频没有评论信息，返回空列表，不报错
	if result.RowsAffected == 0 {
		log.Println("CommentDao-GetCommentList: no comments!!!") //函数返回提示无评论
		return nil, nil
	}
	//若获取评论列表出错
	if result.Error != nil {
		log.Println(result.Error.Error())
		log.Println("CommentDao-GetCommentList: get comment list failed") //函数返回提示获取评论错误
		return nil, errors.New("get comment list failed")
	}

	return commentList, nil
}

// 根据评论ID获取评论信息
func GetCommentInfoById(cid int64) TableComment {
	var res TableComment
	err := Db.Model(TableComment{}).Where("id = ?", cid).First(&res).Error
	if err != nil {
		log.Println("无法获取评论信息")
		return TableComment{}
	} else {
		return res
	}
}
