package dao

import (
	"fmt"
	"testing"
)

// func TestGetTableUserList(t *testing.T) {
// 	list, err := GetTableUserList()
// 	fmt.Printf("%v", list)
// 	fmt.Printf("%v", err)
// }

func TestGetLikeUserIdList(t *testing.T) {
	Init()
	list, err := GetLikeUserIdList(15)
	fmt.Printf("%v", list)
	fmt.Printf("%v", err)
}

func TestGetLikeVideoIdList(t *testing.T) {
	Init()
	list, err := GetLikeVideoIdList(63)
	fmt.Printf("%v", list)
	fmt.Printf("%v", err)
}

func TestGetVideoByVideoId(t *testing.T) {
	Init()
	list, err := GetVideoByVideoId(13)
	fmt.Printf("%v", list)
	fmt.Printf("%v", err)
}

func TestGetFollowingCnt(t *testing.T) {
	Init()
	list, err := GetFollowingCnt(63)
	fmt.Printf("%v", list)
	fmt.Printf("%v", err)
}

func TestGetFollowerCnt(t *testing.T) {
	Init()
	list, err := GetFollowerCnt(63)
	fmt.Printf("%v", list)
	fmt.Printf("%v", err)
}

// func TestInsertTableUser(t *testing.T) {
// 	tu := &TableUser{
// 		Id:       5,
// 		Name:     "a",
// 		Password: "111111",
// 	}
// 	list := InsertTableUser(tu)
// 	fmt.Printf("%v", list)
// }
