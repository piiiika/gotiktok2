package dao

import (
	"fmt"
	"testing"
)

// func TestGetTableUserList(t *testing.T) {
// 	list, err := GetTableUserList()
// 	fmt.Printf("%v", list)
// 	fmt.Printf("%v", err)
// }

func TestGetVideoCount(t *testing.T) {
	Init()
	list, err := GetVideoCountById(63)
	fmt.Printf("%v", list)
	fmt.Printf("%v", err)
}

func TestGetFriendList(t *testing.T) {
	Init()
	list, err := GetFriendList(63)
	fmt.Printf("%v", list)
	fmt.Printf("%v", err)
}

func TestGetFollowerIds(t *testing.T) {
	Init()
	list, err := GetFollowerIds(63)
	fmt.Printf("%v", list)
	fmt.Printf("%v", err)
}

func TestGetFollowingIds(t *testing.T) {
	Init()
	list, err := GetFollowingIds(63)
	fmt.Printf("%v", list)
	fmt.Printf("%v", err)
}

func TestGetVideoCountById(t *testing.T) {
	Init()
	list, err := GetVideoCountById(63)
	fmt.Printf("%v", list)
	fmt.Printf("%v", err)
}
