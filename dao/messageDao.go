package dao

import (
	"Gocode/common"
	"Gocode/config"
	"log"
)

var MessageTime map[int64]int64 = make(map[int64]int64)

type Message struct {
	ID         int64  `json:"id"`
	UserId     int64  `json:"from_user_id"`
	ToUserId   int64  `json:"to_user_id"`
	Content    string `json:"content"`
	CreateTime int64  `json:"create_time" gorm:"column:createTime"` // 创建时间
}

// 根据id查询聊天记录
func QueryMessageByUserId(userId int64) ([]Message, error) {
	var messages []Message
	//SELECT * FROM `messages` WHERE user_id = 1 AND is_withdraw = 0 LIMIT 10
	result := Db.Table("message").
		Where("user_id = ?", userId, 0).Limit(config.MessageCount).Find(&messages)
	if result.Error != nil {
		log.Println(result.Error)
		return nil, result.Error
	}
	return messages, nil
}

// 查询两者的全部聊天记录
func GetMessageByUserIdAndToUserId(userId int64, toUserId int64, preMsgTime int64) ([]common.MessageDto, error) {
	var messages []common.MessageDto
	lastSearchTime := MessageTime[userId]
	// SELECT * FROM `messages` WHERE (user_id = 1 AND to_user_id = 2) OR (user_id = 2 AND to_user_id = 1) ORDER BY createTime asc
	result := Db.Table("message").
		Where("from_user_id = ? AND to_user_id = ? AND create_time > ?", userId, toUserId, lastSearchTime).
		Or("from_user_id = ? AND to_user_id = ? AND create_time > ?", toUserId, userId, lastSearchTime).
		Order("create_time asc").Find(&messages)
	if result.Error != nil {
		log.Println(result.Error)
		return messages, result.Error
	}
	if len(messages) > 0 {
		MessageTime[userId] = messages[len(messages)-1].CreateTime
	}
	return messages, nil
}

// 添加一条信息
func InsertMessage(fromUserId int64, toUserId int64, nowTime int64, content string) error {
	message := common.MessageDto{
		FromUserID: fromUserId,
		ToUserID:   toUserId,
		Content:    content,
		CreateTime: nowTime,
	}
	err := Db.Table("message").Create(&message).Error
	return err
}
