package dao

import (
	// "fmt"
	"errors"
	"log"
	"time"

	"gorm.io/gorm"
)

// TableVideo 对应数据库Video表结构的结构体
type TableVideo struct {
	Id            int64
	PlayUrl       string
	CoverUrl      string
	CommentCount  int64
	FavoriteCount int64
	Title         string
	PublishTime   time.Time
	PusherId      int64
}

// TableName 修改表名映射
func (tableVideo TableVideo) TableName() string {
	return "video"
}

// GetTableVideoByTime 根据latestTime获得TableVideo对象
func GetTableVideoByTime(latestTime string) ([]TableVideo, error) {
	var tableVideos []TableVideo
	if err := Db.Where("publish_time <= ?", latestTime).Order("publish_time desc").Limit(30).Find(&tableVideos).Error; err != nil {
		log.Println(err.Error())
		return tableVideos, err
	}
	return tableVideos, nil
}

// GetTableVideoByUserId 根据latestTime获得TableVideo对象
func GetTableVideoByUserId(userId string) ([]TableVideo, error) {
	var tableVideos []TableVideo
	if err := Db.Where("pusher_id = ?", userId).Order("publish_time desc").Find(&tableVideos).Error; err != nil {
		log.Println(err.Error())
		return tableVideos, err
	}
	return tableVideos, nil
}

func CreatVideo(video TableVideo) error {
	return Db.Create(&video).Error
}

// 根据视频ID获取视频信息
func GetVideoByVideoId(videoID int64) (*TableVideo, error) {
	var videoInfo TableVideo
	err := Db.Where("id = ?", videoID).Take(&videoInfo).Error
	if errors.Is(err, gorm.ErrRecordNotFound) { //异常
		log.Println("未查询到视频数据")
		return nil, err
	} else if err != nil {
		return nil, err
	} else {
		return &videoInfo, nil
	}
}

// 根据发布人ID获取视频发布数量
func GetVideoCountById(Id int64) (int64, error) {
	var videoCount int64
	err := Db.Table("video").Where("pusher_id = ?", Id).Count(&videoCount).Error
	if err != nil {
		return 0, err
	} else {
		return videoCount, nil
	}
}
