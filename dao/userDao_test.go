package dao

import (
	"fmt"
	"log"
	"testing"
)

// func TestGetTableUserList(t *testing.T) {
// 	list, err := GetTableUserList()
// 	fmt.Printf("%v", list)
// 	fmt.Printf("%v", err)
// }

func TestGetTableUserByUsername(t *testing.T) {
	Init()
	list, err := GetTableUserByUsername("test")
	fmt.Printf("%v", list)
	fmt.Printf("%v", err)
}

func TestGetTableUserById(t *testing.T) {
	Init()
	list, err := GetTableUserById(int64(1))
	fmt.Printf("%v", list)
	fmt.Printf("%v", err)
	log.Printf("")
}

// func TestInsertTableUser(t *testing.T) {
// 	tu := &TableUser{
// 		Id:       5,
// 		Name:     "a",
// 		Password: "111111",
// 	}
// 	list := InsertTableUser(tu)
// 	fmt.Printf("%v", list)
// }
