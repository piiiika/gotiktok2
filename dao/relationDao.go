package dao

import (
	"errors"
	"fmt"
	"log"

	"gorm.io/gorm"
)

type TableFollow struct {
	gorm.Model
	UserId     int64
	FollowerId int64
	IsFollow   int8 //是否关注，1-关注，2-未关注
}

type TableLike struct {
	gorm.Model
	UserId  int64
	VideoId int64
	IsLike  int8 //是否点赞，1-点赞，2-未点赞
}

// 修改关注列表表名映射
func (tablefoloow TableFollow) TableName() string {
	return "follow_lists"
}

// 修改喜欢列表表名映射
func (tablelike TableLike) TableName() string {
	return "likes"
}

// 把dao层看成整体，把dao的curd封装在一个结构体中。
type RelationDao struct {
}

// var (
// 	relationDao  *RelationDao //操作该dao层crud的结构体变量。
// 	relationOnce sync.Once  //单例限定，去限定申请一个结构体变量。
// )

// // 生成单例对象。
// func NewDaoInstance() *RelationDao {
// 	relationOnce.Do(
// 		func() {
// 			relationDao = &RelationDao{}
// 		})
// 	return relationDao
// }

// 给定当前用户和目标用户id，查询是否关注。
func FindRelation(userId int64, targetId int64) (*TableFollow, error) {
	//存储用户关系
	get_follow := TableFollow{}

	//查询错误
	if err := Db.
		Where("user_id = ?", targetId).
		Where("follower_id = ?", userId).
		Where("is_follow = ?", 1).
		Take(&get_follow).Error; nil != err {
		// 当没查到数据时，gorm也会报错。
		if gorm.ErrRecordNotFound.Error() == err.Error() {
			return nil, nil
		}
		log.Println(err.Error())
		return nil, err
	}

	return &get_follow, nil
}

// 给定当前用户id，查询表中该用户的粉丝数。
func GetFollowerCnt(userId int64) (int64, error) {
	// 用于存储当前用户粉丝数的变量
	var cnt int64
	// 当查询出现错误的情况，日志打印err msg，并返回err.
	if err := Db.
		Model(TableFollow{}).
		Where("follower_id = ?", userId).
		Where("is_follow = ?", 1).
		Count(&cnt).Error; nil != err {
		log.Println(err.Error())
		return 0, err
	}
	// 正常情况，返回取到的粉丝数。
	return cnt, nil
}

// 给定用户和目标用户的id，更新他们的关系为取消关注或再次关注。
func UpdateFollowRelation(userId int64, targetId int64, isfollow int8) (bool, error) {
	// 更新失败，返回错误。
	if err := Db.Model(TableFollow{}).
		Where("user_id = ?", userId).
		Where("follower_id = ?", targetId).
		Update("is_follow", isfollow).Error; nil != err {
		// 更新失败，打印错误日志。
		log.Println(err.Error())
		return false, err
	}
	// 更新成功。
	return true, nil
}

// 给定用户和目标对象id，插入其关注关系。
func InsertFollowRelation(userId int64, targetId int64) (bool, error) {
	// 生成需要插入的关系结构体。
	follow := TableFollow{
		UserId:     userId,
		FollowerId: targetId,
		IsFollow:   1,
	}
	// 插入失败，返回err.
	err := Db.Select("user_id", "follower_id", "is_follow").Create(&follow).Error
	if nil != err {
		log.Println(err.Error())
		return false, err
	}
	// 插入成功
	return true, nil
}

// 给定当前用户id，查询follow表中该用户关注了多少人。
func GetFollowingCnt(userId int64) (int64, error) {
	// 用于存储当前用户关注了多少人。
	var cnt int64
	// 查询出错，日志打印err msg，并return err
	if err := Db.Model(TableFollow{}).
		Where("user_id = ?", userId).
		Where("is_follow = ?", 1).
		Count(&cnt).Error; nil != err {
		log.Println(err.Error())
		return 0, err
	}
	// 查询成功，返回人数。
	return cnt, nil
}

// 给定当前用户和目标用户id，查看曾经是否有关注关系。
func GetEverFollowing(userId int64, targetId int64) (*TableFollow, error) {
	// 用于存储查出来的关注关系。
	follow := TableFollow{}
	//当查询出现错误时，日志打印err msg，并return err.
	if err := Db.
		Where("user_id = ?", userId).
		Where("follower_id = ?", targetId).
		Where("is_follow = ? or is_follow = ?", 1, 2).
		Take(&follow).Error; nil != err {
		// 当没查到记录报错时，不当做错误处理。
		if gorm.ErrRecordNotFound.Error() == err.Error() {
			return nil, nil
		}
		log.Println(err.Error())
		return nil, err
	}
	//正常情况，返回取到的关系和空err.
	return &follow, nil
}

// 查询关注的用户id。
func GetFollowingIds(userId int64) ([]int64, error) {
	var ids []int64
	if err := Db.
		Model(TableFollow{}).
		Where("user_id = ? AND is_follow = 1", userId).
		Pluck("follower_id", &ids).Error; nil != err {
		log.Printf("查询关注的用户id,%#v", ids)
		// 没有关注任何人，但是不能算错。
		if gorm.ErrRecordNotFound.Error() == err.Error() {
			return nil, nil
		}
		// 查询出错。

		return nil, err
	}
	// 查询成功。
	return ids, nil
}

// 查询所有粉丝ID。
func GetFollowerIds(userId int64) ([]int64, error) {
	var ids []int64
	if err := Db.
		Model(TableFollow{}).
		Where("follower_id = ? AND is_follow = 1", userId).
		Pluck("user_id", &ids).Error; nil != err {
		// 没有关注任何人
		if gorm.ErrRecordNotFound.Error() == err.Error() {
			return nil, nil
		}
		// 查询出错。
		log.Println(err.Error())
		return nil, err
	}
	// 查询成功。
	return ids, nil
}

// 查询好友列表
func GetFriendList(userId int64) ([]int64, error) {
	MessageTime[userId] = 0
	ids, err := GetFollowerIds(userId)
	if err != nil {
		return nil, err
	}
	ids2, err := GetFollowingIds(userId)
	if err != nil {
		return nil, err
	}

	mp := make(map[int64]bool)

	for _, value := range ids {
		mp[value] = true
	}
	var res []int64
	for _, value := range ids2 {
		if mp[value] {
			fmt.Println("append")
			res = append(res, value)
		}
	}
	// 查询成功。
	return res, nil
}

// 更新是否点赞操作
func UpdateLikeAction(user_Id int64, vedio_Id int64, islike int8) (bool, error) {
	// 更新失败，返回错误。
	if err := Db.Model(TableFollow{}).
		Where("user_id = ?", user_Id).
		Where("follower_id = ?", vedio_Id).
		Update("is_follow", islike).Error; nil != err {
		// 更新失败，打印错误日志。
		log.Println(err.Error())
		return false, err
	}
	// 更新成功。
	return true, nil

}

// 查询历史点赞信息
func GetEverLike(user_Id int64, video_Id int64) (*TableLike, error) {
	// 用于存储查出来的关注关系。
	like := TableLike{}
	//当查询出现错误时，日志打印err msg，并return err.
	if err := Db.
		Where("user_id = ?", user_Id).
		Where("video_id = ?", video_Id).
		Where("is_like = ? or is_like = ?", 1, 2).
		Take(&like).Error; nil != err {
		// 当没查到记录报错时，不当做错误处理。
		if gorm.ErrRecordNotFound.Error() == err.Error() {
			return nil, nil
		}
		log.Println(err.Error())
		return nil, err
	}
	//正常情况，返回取到的关系和空err.
	return &like, nil
}

// 插入点赞数据
func InsertLike(userId int64, videoId int64) error {
	//构建点赞数据
	likeData := TableLike{
		UserId:  userId,
		VideoId: videoId,
		IsLike:  1,
	}

	//创建点赞数据，默认为点赞，返回错误结果
	err := Db.Model(TableLike{}).Create(&likeData).Error
	//如果有错误结果，返回插入失败
	if err != nil {
		log.Println(err.Error())
		return err
	}
	return nil
}

// //根据userId,videoId查询点赞信息
// func GetLikeInfo(userId int64, videoId int64) (TableLike, error) {
// 	//创建一条空like结构体，用来存储查询到的信息
// 	var likeInfo TableLike
// 	//根据userid,videoId查询是否有该条信息，如果有，存储在likeInfo,返回查询结果
// 	err := Db.Model(TableLike{}).Where(map[string]interface{}{"user_id": userId, "video_id": videoId}).
// 		First(&likeInfo).Error
// 	if err != nil {
// 		//查询数据为0
// 		if gorm.ErrRecordNotFound.Error() == err.Error() {
// 			log.Println("can't find data")
// 			return TableLike{}, nil
// 		} else {
// 			//如果查询数据库失败，返回获取likeInfo信息失败
// 			log.Println(err.Error())
// 			return likeInfo, err
// 		}
// 	}
// 	return likeInfo, nil
// }

// 查询点赞视频ID列表
func GetLikeVideoIdList(userId int64) ([]int64, error) {
	var likeVideoIdList []int64
	err := Db.Model(TableLike{}).Where(map[string]interface{}{"user_id": userId, "is_like": 1}).
		Pluck("video_id", &likeVideoIdList).Error
	if err != nil {
		//查询数据为0，返回空likeVideoIdList切片，以及返回无错误
		if gorm.ErrRecordNotFound.Error() == err.Error() {
			log.Println("there are no likeVideoId")
			return likeVideoIdList, nil
		} else {
			//如果查询数据库失败，返回获取likeVideoIdList失败
			log.Println(err.Error())
			return likeVideoIdList, errors.New("get likelist fail")
		}
	}
	return likeVideoIdList, nil
}

func GetFavoriteCount(videoId int64) (int64, error) {
	var favoriteCount int64
	err := Db.Model(TableLike{}).
		Where(map[string]interface{}{"video_id": videoId, "is_like": 1}).
		Count(&favoriteCount).Error
	if err != nil {
		log.Println(err)
	}
	return favoriteCount, err
}

// 根据videoId获取点赞userId
func GetLikeUserIdList(videoId int64) ([]int64, error) {
	var likeUserIdList []int64 //存所有该视频点赞用户id；
	//查询likes表对应视频id点赞用户，返回查询结果
	err := Db.Model(TableLike{}).Where(map[string]interface{}{"video_id": videoId, "is_like": 1}).
		Pluck("user_id", &likeUserIdList).Error
	//查询过程出现错误，返回默认值0，并输出错误信息
	if err != nil {
		log.Println(err.Error())
		return nil, errors.New("get likeUserIdList failed")
	} else {
		//没查询到或者查询到结果，返回数量以及无报错
		return likeUserIdList, nil
	}
}

// 判断用户间是否相互关注
func IsfollowEach(current_userId int64, other_userId int64) (bool, error) {
	//TODO
	return false, nil

}
