package dao

import (
	"fmt"
	"testing"
	"time"
)


func TestSendComment(t *testing.T) {
	Init()
	var comment TableComment
	comment.CommentText = "测试测试测试25"
	comment.CreateDate = time.Now()
	comment.UserId = 2
	comment.VideoId = 10
	res, err := InsertComment(comment)
	fmt.Printf("%v", res)
	fmt.Printf("%v", err)
}

func TestGetCommentList(t *testing.T) {
	Init()

	res, err := GetCommentList(10)
	fmt.Printf("%v", res)
	fmt.Printf("%v", err)
}

func TestDeleteComment(t *testing.T) {
	Init()
	// var comment TableComment
	// comment.CommentText = "测试测试测试2"
	// comment.CreateDate = time.Now()
	// comment.UserId = 1
	// comment.VideoId = 10
	err := DeleteComment(2)
	// fmt.Printf("%v", res)
	fmt.Printf("%v", err)
}
