package dao

import (
	"fmt"
	"testing"
	"time"
)

// func TestGetTableUserList(t *testing.T) {
// 	list, err := GetTableUserList()
// 	fmt.Printf("%v", list)
// 	fmt.Printf("%v", err)
// }

func TestGetMessageByUserIdAndToUserId(t *testing.T) {
	Init()
	list, err := GetMessageByUserIdAndToUserId(63, 64, 12312341)
	fmt.Printf("%v", list)
	fmt.Printf("%v", err)
}

func TestInsertMessage(t *testing.T) {
	Init()
	err := InsertMessage(64, 63, time.Now().Unix(), "？？？？？？")
	fmt.Printf("%#v", err)
}

// func TestInsertTableUser(t *testing.T) {
// 	tu := &TableUser{
// 		Id:       5,
// 		Name:     "a",
// 		Password: "111111",
// 	}
// 	list := InsertTableUser(tu)
// 	fmt.Printf("%v", list)
// }
