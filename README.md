## 运行程序

`go run .`

## 环境依赖
- FFmpeg
```bash
# CentOS8安装命令
sudo yum install epel-release -y
sudo yum update -y
sudo rpm --import http://li.nux.ro/download/nux/RPM-GPG-KEY-nux.ro
sudo rpm -Uvh http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
sudo yum install ffmpeg ffmpeg-devel -y
# 测试
ffmpeg
```
## 说明
图片和视频都在服务器上存储，如果更换请修改`baseUrl`