create table follow_lists
(
    id          bigint unsigned auto_increment
        primary key,
    created_at  datetime(3)       null,
    updated_at  datetime(3)       null,
    deleted_at  datetime(3)       null,
    user_id     bigint            not null,
    follower_id bigint            not null,
    is_follow   tinyint default 1 null
);

create index idx_follow_lists_deleted_at
    on follow_lists (deleted_at);

create table likes
(
    id         bigint unsigned auto_increment
        primary key,
    created_at datetime(3)       null,
    updated_at datetime(3)       null,
    deleted_at datetime(3)       null,
    user_id    bigint            not null,
    video_id   bigint            not null,
    is_like    tinyint default 1 null
);

create index idx_likes_deleted_at
    on likes (deleted_at);

create table message
(
    id          bigint unsigned auto_increment
        primary key,
    user_id     bigint                              not null,
    to_user_id  bigint                              not null,
    content     varchar(255)                        not null,
    create_time timestamp default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment 'timestamp类型会比较慢，如果追求效率也可以用bigint类型，不过代码转换比较麻烦',
    constraint primarykey
        unique (id) comment '对id的主键索引
',
    constraint to_user_id_index
        unique (to_user_id, user_id),
    constraint user_id_index
        unique (user_id, to_user_id)
)
    charset = utf8mb3;

create table user_focus
(
    follow_id   bigint not null,
    follower_id bigint not null,
    primary key (follow_id, follower_id),
    constraint follow_index
        unique (follow_id, follower_id),
    constraint follower_index
        unique (follower_id, follow_id)
)
    charset = utf8mb3;

create table users
(
    id             bigint unsigned auto_increment
        primary key,
    username       varchar(100)                not null,
    password       varchar(100)                not null,
    follow_count   bigint unsigned default '0' null,
    follower_count bigint unsigned default '0' null,
    constraint primarykey
        unique (id) comment '主键索引',
    constraint username
        unique (username) comment '用户名索引'
)
    charset = utf8mb3;

create table video
(
    id             bigint auto_increment
        primary key,
    play_url       varchar(300)                not null,
    cover_url      varchar(300)                not null,
    favorite_count bigint unsigned default '0' not null,
    comment_count  bigint unsigned default '0' not null,
    title          varchar(32)                 not null,
    publish_time   timestamp                   not null,
    pusher_id      bigint                      not null,
    constraint primary_index
        unique (id)
)
    charset = utf8mb3;

create index push_id_index
    on video (pusher_id);


