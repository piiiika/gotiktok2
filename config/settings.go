package config

//jwt密钥 用于token的生成
var JwtSecret = "Don'tWantToStayUpLate"

//个人信息页面，头像和背景大图以及签名
var DefaultSignature = "Don'tWantToStayUpLate"
var DefaultAvater = "https://tupian.qqw21.com/article/UploadPic/2020-10/202010272223839574.jpg"
var DefaultBackGround = "https://p.qqan.com/up/2020-9/15992721866344541.jpg"

//一次消息最多返回多少条数据
var MessageCount = 20
