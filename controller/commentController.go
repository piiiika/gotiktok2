package controller

import (
	"Gocode/common"
	"Gocode/dao"
	"Gocode/service"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

//用户评论返回体
type CommentDto struct {
	Comment    *common.Comment `json:"comment"`    // 评论成功返回评论内容，不需要重新拉取整个列表
	StatusCode int64    `json:"status_code"`// 状态码，0-成功，其他值-失败
	StatusMsg  string  `json:"status_msg"` // 返回状态描述
}

//评论列表
type CommentList struct {
	CommentList []common.Comment `json:"comment_list"`// 评论列表
	StatusCode  int64     `json:"status_code"` // 状态码，0-成功，其他值-失败
	StatusMsg   string   `json:"status_msg"`  // 返回状态描述
}

//评论操作(发布 OR 删除)
func CommentAction(c *gin.Context){
	userId, _ := service.ParseTokenID(c.Query("token"))
	videoid, _ := strconv.ParseInt(c.Query("video_id"), 10, 64)
	actionType,_ := strconv.ParseInt(c.Query("action_type"), 10, 64)
	csi := service.CommentServiceImpl{}
	if actionType == 1{	//发布评论
		//获取评论信息
		commentString := c.Query("comment_text")

		//数据封装
		var comment dao.TableComment
		comment.CommentText = commentString
		comment.UserId = userId
		comment.VideoId = videoid
		
		comment.CreateDate = time.Now()
		//发布
		res,err := csi.SendComment(comment)
		if err != nil{
			c.JSON(http.StatusExpectationFailed, CommentDto{
				Comment: nil,
				StatusCode: -1,
				StatusMsg: "服务错误,发送失败",
			})
		}else {
			c.JSON(http.StatusOK, CommentDto{
				Comment: &res,
				StatusCode: 0,
				StatusMsg: "发布评论成功",
			})
		}
	}else if actionType == 2 {	//删除评论
		cid, _ := strconv.ParseInt(c.Query("comment_id"), 10, 64) //评论id
		err := csi.DeleteComment(cid)
		if err != nil{
			c.JSON(http.StatusExpectationFailed, CommentDto{
				Comment: nil,
				StatusCode: -1,
				StatusMsg: err.Error(),
			})
		}else {
			c.JSON(http.StatusOK, CommentDto{
				Comment: nil,
				StatusCode: 0,
				StatusMsg: "删除成功",
			})
		}
	}else {	//请求参数错误
		c.JSON(http.StatusExpectationFailed, CommentDto{
			Comment: nil,
			StatusCode: -1,
			StatusMsg: "请求参数错误",
		})
	}	
}

//获取评论列表
func GetCommentList(c *gin.Context){
	// userId, _ := service.ParseTokenID(c.Query("token"))
	videoid, _ := strconv.ParseInt(c.Query("video_id"), 10, 64)

	csi := service.CommentServiceImpl{}
	res,err := csi.GetCommentList(videoid)
	if err != nil{
		c.JSON(http.StatusExpectationFailed, CommentList{
			CommentList: nil,
			StatusCode: -1,
			StatusMsg: "获取评论列表失败",
		})
	}else {
		c.JSON(http.StatusOK, CommentList{
			CommentList: res,
			StatusCode: 0,
			StatusMsg: "获取评论列表成功",
		})
	}

}