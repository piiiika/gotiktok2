package controller

import (
	"Gocode/common"
	"Gocode/dao"
	"Gocode/service"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type Response struct {
	StatusCode int32  `json:"status_code"`
	StatusMsg  string `json:"status_msg,omitempty"`
}

type UserLoginResponse struct {
	Response
	UserId int64  `json:"user_id,omitempty"`
	Token  string `json:"token"`
}

type UserResponse struct {
	Response
	User common.UserDto `json:"user"`
}

var User_Id int64

// Login POST douyin/user/login/ 用户登录
func Login(c *gin.Context) {
	fmt.Print("douyin/user/login/")
	username := c.Query("username")
	password := c.Query("password")
	// encoderPassword := service.EnCoder(password)
	// println(encoderPassword)

	usi := service.UserServiceImpl{}

	u := usi.GetTableUserByUsername(username)
	flg := service.CheckPassword(u.Password, password)

	if flg {
		token, err := service.GenerateToken(username, u.Id)
		if err != nil {
			c.JSON(http.StatusOK, UserLoginResponse{
				Response: Response{StatusCode: 1, StatusMsg: "Token Generate Error"},
			})
		}
		c.JSON(http.StatusOK, UserLoginResponse{
			Response: Response{StatusCode: 0},
			UserId:   u.Id,
			Token:    token,
		})
	} else {
		c.JSON(http.StatusOK, UserLoginResponse{
			Response: Response{StatusCode: 1, StatusMsg: "Username or Password Error"},
		})
	}
}

// GET /douyin/user 获取用户信息
func UserInfo(c *gin.Context) {
	user_id := c.Query("user_id")
	id, _ := strconv.ParseInt(user_id, 10, 64)

	// userId := c.GetInt64("userId")
	// userName := c.GetString("userName")
	// log.Printf("userId = %v", userId) //userId是id值
	// log.Printf("username = %v", userName)

	User_Id = id //全局用户id变量

	usi := service.UserServiceImpl{}

	if u, err := usi.GetUserById(id); err != nil {
		c.JSON(http.StatusOK, UserResponse{
			Response: Response{StatusCode: 1, StatusMsg: "User Doesn't Exist"},
		})
	} else {
		c.JSON(http.StatusOK, UserResponse{
			Response: Response{StatusCode: 0},
			User:     u,
		})
	}
}

// POST /douyin/user/register 用户注册
func Regist(c *gin.Context) {
	username := c.Query("username")
	password := c.Query("password")

	usi := service.UserServiceImpl{}

	u := usi.GetTableUserByUsername(username)

	if username == u.Username {
		c.JSON(http.StatusOK, UserLoginResponse{
			Response: Response{StatusCode: 1, StatusMsg: "User already exist"},
		}) //用户已存在
	} else {
		newUser := dao.TableUser{
			Username: username,
			Password: service.EnCoder(password),
		}
		if !usi.InsertTableUser(&newUser) {
			println("Insert Data Fail")
		}
		u := usi.GetTableUserByUsername(username)
		token := "success"
		log.Println("注册返回的id: ", u.Id)
		c.JSON(http.StatusOK, UserLoginResponse{
			Response: Response{StatusCode: 0},
			UserId:   u.Id,
			Token:    token,
		})
	}
}
