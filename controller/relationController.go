package controller

import (
	"Gocode/common"
	"Gocode/service"

	// "fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

// 关注和取消关注需要返回结构。
type RelationActionResp struct {
	Response
}

// 获取关注列表需要返回的结构。
type FollowResp struct {
	Response
	UserList []common.UserDto `json:"user_list,omitempty"`
}

// 获取点赞列表需要返回的结构。
type LikeResp struct {
	Response
	VedioList []common.VideoDto `json:"video_list,omitempty"`
}

// 关注其它用户
func ActionById(c *gin.Context) {
	// userId, err1 := strconv.ParseInt(c.GetString("user_id"), 10, 64)
	userId, _ := service.ParseTokenID(c.Query("token"))
	toUserId, err2 := strconv.ParseInt(c.Query("to_user_id"), 10, 64)
	actionType, err3 := strconv.ParseInt(c.Query("action_type"), 10, 64)

	log.Println(userId)
	log.Println(toUserId)
	log.Println(actionType)

	// 传入参数格式有问题。
	if userId < 1 || nil != err2 || nil != err3 || actionType < 1 || actionType > 2 {
		log.Println("fail")
		ErrorHandler(c, 2, "参数格式错误")
		return
	}
	fsi := service.RelationServiceImpl{}

	res, err := fsi.ActionById(userId, toUserId, int8(actionType))
	if res {
		log.Println("关注、取关成功。")
		c.JSON(http.StatusOK, RelationActionResp{
			Response{
				StatusCode: 0,
				StatusMsg:  "OK",
			},
		})
	} else {
		c.JSON(http.StatusExpectationFailed, RelationActionResp{
			Response{
				StatusCode: -1,
				StatusMsg:  err.Error(),
			},
		})
	}
}

// 获取关注列表
func GetFollowingList(c *gin.Context) {
	userId, err := strconv.ParseInt(c.Query("user_id"), 10, 64)
	// 用户id解析出错。
	if nil != err {
		c.JSON(http.StatusExpectationFailed, FollowResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  "用户id格式错误。",
			},
			UserList: nil,
		})
		return
	}

	rsi := service.RelationServiceImpl{}
	getUsers, err2 := rsi.GetFollowing(userId)

	// 获取关注列表时出错。
	if err2 != nil {
		c.JSON(http.StatusExpectationFailed, FollowResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  "获取关注列表出错。",
			},
			UserList: nil,
		})
		return
	} else if getUsers != nil {
		c.JSON(http.StatusOK, FollowResp{
			Response: Response{
				StatusCode: 0,
				StatusMsg:  "OK",
			},
			UserList: getUsers,
		})
		return
	} else {
		c.JSON(http.StatusExpectationFailed, FollowResp{
			Response: Response{
				StatusCode: 0,
				StatusMsg:  "following_List null",
			},
			UserList: getUsers,
		})
	}

}

// 获取粉丝列表
func GetFollowerList(c *gin.Context) {
	userId, err := strconv.ParseInt(c.Query("user_id"), 10, 64)
	if nil != err {
		ErrorHandler(c, 1, "用户ID格式错误")
	}

	rsi := service.RelationServiceImpl{}

	getUsers, err2 := rsi.GetFollowers(userId)

	// 获取关注列表时出错。
	if err2 != nil {
		c.JSON(http.StatusExpectationFailed, FollowResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  "获取关注列表出错。",
			},
			UserList: nil,
		})
		return
	} else if getUsers != nil {
		c.JSON(http.StatusOK, FollowResp{
			Response: Response{
				StatusCode: 0,
				StatusMsg:  "OK",
			},
			UserList: getUsers,
		})
		return
	} else {
		c.JSON(http.StatusOK, FollowResp{
			Response: Response{
				StatusCode: 0,
				StatusMsg:  "followier_List null",
			},
			UserList: getUsers,
		})
	}

}

// 获取好友列表
func GetFriendList(c *gin.Context) {
	userId, err := strconv.ParseInt(c.Query("user_id"), 10, 64)
	if nil != err {
		ErrorHandler(c, 1, "用户ID格式错误")
	}

	rsi := service.RelationServiceImpl{}

	getFriendList, err2 := rsi.GetFriendList(userId)
	if err2 != nil {
		c.JSON(http.StatusExpectationFailed, FollowResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  "获取粉丝列表出错",
			},
			UserList: nil,
		})
	} else if getFriendList != nil {
		c.JSON(http.StatusOK, FollowResp{
			Response: Response{
				StatusCode: 0,
				StatusMsg:  "OK",
			},
			UserList: getFriendList,
		})
		return
	} else {
		c.JSON(http.StatusOK, FollowResp{
			Response: Response{
				StatusCode: 0,
				StatusMsg:  "friend_list null",
			},
			UserList: getFriendList,
		})
	}
}

// 操作点赞
func ActionLike(c *gin.Context) {
	userId := c.GetInt64("userId")
	// userId, err1 := strconv.ParseInt(c.Query("user_id"), 10, 64)
	videoId, err2 := strconv.ParseInt(c.Query("video_id"), 10, 64)
	actionType, err3 := strconv.ParseInt(c.Query("action_type"), 10, 64)

	if nil != err2 || nil != err3 {
		ErrorHandler(c, 2, "参数类型错误")
	}

	rsi := service.RelationServiceImpl{}
	action, err := rsi.FavouriteAction(userId, videoId, int8(actionType))
	if err != nil {
		c.JSON(http.StatusExpectationFailed, RelationActionResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  err.Error(),
			},
		})
	} else if !action {
		c.JSON(http.StatusExpectationFailed, RelationActionResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  "Like Action Fail",
			},
		})
	} else {
		c.JSON(http.StatusOK, RelationActionResp{
			Response: Response{
				StatusCode: 0,
				StatusMsg:  "Like Action Success",
			},
		})
	}

}

// 获取点赞(喜欢)列表
func GetLikeList(c *gin.Context) {
	cur_userId, uerr := strconv.ParseInt(c.Query("user_id"), 10, 64)
	if uerr != nil {
		ErrorHandler(c, 3, "参数类型错误")
	}

	rsi := service.RelationServiceImpl{}
	res, err := rsi.GetFavouriteList(cur_userId)
	if err != nil {
		ErrorHandler(c, 3, err.Error())
	} else {
		c.JSON(http.StatusOK, LikeResp{
			Response: Response{
				StatusCode: 0,
				StatusMsg:  "Like Action Success",
			},
			VedioList: res,
		})
	}

}

// 错误处理
func ErrorHandler(c *gin.Context, errtype int, meg string) {
	switch errtype {
	case 1:
		c.JSON(http.StatusExpectationFailed, FollowResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  meg,
			},
			UserList: nil,
		})
	case 2:
		c.JSON(http.StatusExpectationFailed, RelationActionResp{
			Response{
				StatusCode: -2,
				StatusMsg:  meg,
			},
		})
	case 3:
		c.JSON(http.StatusExpectationFailed, LikeResp{
			Response: Response{
				StatusCode: -3,
				StatusMsg:  meg,
			},
			VedioList: nil,
		})
	}

}
