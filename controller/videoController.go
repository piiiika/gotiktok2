package controller

import (
	"Gocode/common"
	"Gocode/service"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

type VideoResponse struct {
	Response
	NextTime  *int64            `json:"next_time,omitempty"` // 本次返回的视频中，发布最早的时间，作为下次请求时的latest_time
	VideoList []common.VideoDto `json:"video_list"`          // 视频列表
}

// Feed PUT douyin/feed/ 视频流接口，不限制登录状态，返回按投稿时间倒序的视频列表，视频数由服务端控制，单次最多30个
func Feed(c *gin.Context) {
	beforeTime, _ := time.Parse("2006-01-02 15:04:05", "2000-01-01 00:00:00")
	fmt.Println("douyin/feed/")
	latestTime := c.Query("latest_time")
	token := c.Query("token")
	fmt.Println("latest_time:", latestTime, ", token:", token)
	if latestTime == "" {
		latestTime = time.Now().Format("2006-01-02 15:04:05")
	} else {
		data, _ := strconv.ParseInt(latestTime, 10, 64)
		fmt.Println(data, beforeTime.UnixNano())
		if data <= beforeTime.UnixNano() {
			latestTime = time.Now().Format("2006-01-02 15:04:05")
		} else {
			latestTime = time.Unix(data/1000000000, 0).Format("2006-01-02 15:04:05")
		}
	}
	fmt.Println("after convert latest_time:", latestTime)

	usi := service.UserServiceImpl{}

	tableVideos := usi.GetVideoByTime(latestTime)
	fmt.Println(tableVideos)
	videoList := usi.ConvertTableVideoToVideoDto(tableVideos)

	nextTime := time.Now().UnixNano()
	// 计算NextTime
	for _, video := range tableVideos {
		if video.PublishTime.UnixNano() <= nextTime {
			nextTime = video.PublishTime.UnixNano()
		}
	}

	c.JSON(http.StatusOK, VideoResponse{
		Response:  Response{StatusCode: 0, StatusMsg: "获取视频成功！"},
		NextTime:  &nextTime,
		VideoList: videoList,
	})
}

// List Get douyin/publish/list/ 发布列表
func List(c *gin.Context) {
	fmt.Print("douyin/publish/list/")
	token := c.Query("token")
	userId := c.Query("user_id")

	fmt.Println("userId:", userId, ", token:", token)
	tokenId, _ := service.ParseTokenID(token)
	// 需要校验token
	if strconv.FormatInt(tokenId, 10) != userId {
		// token不正确，则返回空结果
		c.JSON(http.StatusOK, VideoResponse{
			Response:  Response{StatusCode: 0, StatusMsg: "获取用户id为[" + userId + "]的所有投稿视频失败！token不一致！"},
			VideoList: []common.VideoDto{},
		})
		return
	}

	usi := service.UserServiceImpl{}

	tableVideos := usi.GetVideoByUserId(userId)
	fmt.Println(tableVideos)
	videoList := usi.ConvertTableVideoToVideoDto(tableVideos)

	c.JSON(http.StatusOK, VideoResponse{
		Response:  Response{StatusCode: 0, StatusMsg: "获取用户id为[" + userId + "]的所有投稿视频成功！"},
		VideoList: videoList,
	})
}

func Action(c *gin.Context) {
	token := c.PostForm("token")
	title := c.PostForm("title")

	// 检查token，找到用户，与用户建立联系，存到数据库中
	id, err := service.ParseTokenID(token)
	if err != nil {
		c.JSON(http.StatusOK, Response{
			StatusCode: 1,
			StatusMsg:  err.Error(),
		})
		return
	}
	fmt.Println("token=", token, "title", title, "id=", id)
	// 接收文件
	data, err := c.FormFile("data")
	if err != nil {
		c.JSON(http.StatusOK, Response{
			StatusCode: 1,
			StatusMsg:  err.Error(),
		})
		return
	}

	//filename := filepath.Base(data.Filename)
	//finalName := fmt.Sprintf("%d_%s", user.Id, filename)
	filename := strings.Replace(uuid.New().String(), "-", "", -1) + ".mp4"
	saveFile := filepath.Join("./public/", filename)
	if err := c.SaveUploadedFile(data, saveFile); err != nil {
		c.JSON(http.StatusOK, Response{
			StatusCode: 1,
			StatusMsg:  err.Error(),
		})
		return
	}
	usi := service.UserServiceImpl{}
	err = usi.PublishVideo(id, filename, title)
	if err != nil {
		c.JSON(http.StatusOK, Response{
			StatusCode: 1,
			StatusMsg:  err.Error(),
		})
		return
	}
	c.JSON(http.StatusOK, Response{
		StatusCode: 0,
		StatusMsg:  "上传成功",
	})
}
