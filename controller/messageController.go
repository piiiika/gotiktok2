package controller

import (
	"Gocode/common"
	"Gocode/service"
	"log"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

// FollowingResp 获取关注列表需要返回的结构。
type MessageResp struct {
	Response
	MessageList []common.MessageDto `json:"message_list,omitempty"`
}

// MessageChat 聊天记录 点进去才能看到
func MessageChat(c *gin.Context) {
	to_user_id := c.Query("to_user_id")
	toUserId, err := strconv.ParseInt(to_user_id, 10, 64)
	if to_user_id == "" || nil != err {
		c.JSON(http.StatusOK, MessageResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  "用户id格式错误。",
			},
		})
		return
	}
	log.Println(to_user_id)
	// token校验获得了userid并存入上下文， 提取用户Id
	userId := c.GetInt64("userId")
	// userId := int64(63)

	if userId == 0 {
		c.JSON(http.StatusOK, MessageResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  "信息发送者用户名解析失败",
			},
		})
	}

	// if !exists {
	// 	c.JSON(http.StatusOK, MessageResp{
	// 		Response: Response{
	// 			StatusCode: -1,
	// 			StatusMsg:  "token格式错误。",
	// 		},
	// 	})
	// 	return
	// }
	// 提取上次最新消息的时间
	// pre_msg_time := c.Query("pre_msg_time")
	// preMsgTime, err := strconv.ParseInt(pre_msg_time, 10, 64)
	preMsgTime := int64(0)
	if err != nil {
		c.JSON(http.StatusOK, MessageResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  "信息时间错误。",
			},
		})
		return
	}

	messages, err := service.MessageChatService(userId, toUserId, preMsgTime)
	if err != nil {
		c.JSON(http.StatusOK, MessageResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  "信息获取错误。",
			},
		})
		return
	}
	log.Printf("%#v", messages)
	c.JSON(http.StatusOK, MessageResp{
		Response: Response{
			StatusCode: 0,
			StatusMsg:  "查询成功",
		},
		MessageList: messages,
	})

}

func MessageAction(c *gin.Context) {
	to_user_id := c.Query("to_user_id")
	action_type := c.Query("action_type")
	content := c.Query("content")

	toUserId, err := strconv.ParseInt(to_user_id, 10, 64)
	if err != nil {
		c.JSON(http.StatusOK, MessageResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  "传入to_user_id错误",
			},
		})
	}
	actionType, err := strconv.ParseInt(action_type, 10, 64)
	if actionType != 1 || err != nil {
		c.JSON(http.StatusOK, MessageResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  "action_type 错误",
			},
		})
	}

	userId := c.GetInt64("userId")
	// userId := int64(63)

	if userId == 0 {
		c.JSON(http.StatusOK, MessageResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  "信息发送者用户名解析失败",
			},
		})
	}

	nowTime := time.Now().Unix()

	err = service.MessageAction(userId, toUserId, nowTime, content)
	if err != nil {
		c.JSON(http.StatusOK, MessageResp{
			Response: Response{
				StatusCode: -1,
				StatusMsg:  "信息发送失败。",
			},
		})
		return
	}
	c.JSON(http.StatusOK, MessageResp{
		Response: Response{
			StatusCode: 0,
			StatusMsg:  "信息发送成功",
		},
	})
}
