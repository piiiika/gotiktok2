package common


//User响应体
type UserDto struct {
	Avatar          string `json:"avatar,omitempty"`           // 用户头像
	BackgroundImage string `json:"background_image,omitempty"` // 用户个人页顶部大图
	FavoriteCount   int64  `json:"favorite_count,omitempty"`   // 喜欢数
	FollowCount     int64  `json:"follow_count,omitempty"`     // 关注总数
	FollowerCount   int64  `json:"follower_count"`             // 粉丝总数
	Id              int64  `json:"id"`                         // 用户id
	IsFollow        bool   `json:"is_follow"`                  // true-已关注，false-未关注
	Name            string `json:"name"`                       // 用户名称
	Signature       string `json:"signature"`                  // 个人简介
	TotalFavorited  string `json:"total_favorited,omitempty"`  // 获赞数量
	WorkCount       int64  `json:"work_count,omitempty"`       // 作品数
}

// VideoDto 视频信息
type VideoDto struct {
	Author        UserDto `json:"author"`         // 视频作者信息
	CommentCount  int64   `json:"comment_count"`  // 视频的评论总数
	CoverURL      string  `json:"cover_url"`      // 视频封面地址
	FavoriteCount int64   `json:"favorite_count"` // 视频的点赞总数
	ID            int64   `json:"id"`             // 视频唯一标识
	IsFavorite    bool    `json:"is_favorite"`    // true-已点赞，false-未点赞
	PlayURL       string  `json:"play_url"`       // 视频播放地址
	Title         string  `json:"title"`          // 视频标题
}



// Comment
type Comment struct {
	Content    string `json:"content"`    // 评论内容
	CreateDate string `json:"create_date"`// 评论发布日期，格式 mm-dd
	ID         int64  `json:"id"`         // 评论id
	User       UserDto   `json:"user"`       // 评论用户信息
}

// Message
type MessageDto struct {
	Content    string `json:"content"`      // 消息内容
	CreateTime int64  `json:"create_time"`  // 消息发送时间 yyyy-MM-dd HH:MM:ss
	FromUserID int64  `json:"from_user_id"` // 消息发送者id
	ID         int64  `json:"id"`           // 消息id
	ToUserID   int64  `json:"to_user_id"`   // 消息接收者id
}

//通用响应体
type Response struct {
	StatusCode int32  `json:"status_code"`
	StatusMsg  string `json:"status_msg,omitempty"`
}
