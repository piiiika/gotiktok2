package main

import (
	"Gocode/controller"
	jwt "Gocode/handler"

	"github.com/gin-gonic/gin"
)

func initRouter(r *gin.Engine) {
	// public directory is used to serve static resources
	r.Static("/static", "./public")

	apiRouter := r.Group("/douyin") //设置路由群组

	//用户操作集
	apiRouter.POST("/user/login/", controller.Login)         //登陆
	apiRouter.GET("/user/", jwt.Auth(), controller.UserInfo) //获取用户信息
	apiRouter.POST("/user/register/", controller.Regist)     //注册

	apiRouter.POST("/favorite/action/", jwt.Auth(), controller.ActionLike) //点赞
	apiRouter.POST("/comment/action/", controller.CommentAction)           //评论
	apiRouter.GET("/comment/list/", controller.GetCommentList)            //评论列表

	//视频流接口
	apiRouter.GET("/feed/", jwt.AuthWithoutLogin(), controller.Feed) //获取
	apiRouter.GET("/publish/list/", jwt.Auth(), controller.List)     //发表列表
	apiRouter.POST("/publish/action/", controller.Action)            //发布视频

	//用户社交关系
	apiRouter.POST("/relation/action/", jwt.Auth(), controller.ActionById)            //关注操作
	apiRouter.GET("/relation/follow/list/", jwt.Auth(), controller.GetFollowingList)  //关注列表
	apiRouter.GET("/relation/follower/list/", jwt.Auth(), controller.GetFollowerList) //粉丝列表
	apiRouter.GET("/relation/friend/list/", jwt.Auth(), controller.GetFriendList)     //好友列表
	apiRouter.GET("/favorite/list/", jwt.Auth(), controller.GetLikeList)              //喜欢列表

	//用户消息关系
	// 聊天记录
	apiRouter.GET("/message/chat/", jwt.Auth(), controller.MessageChat)
	// 发送消息
	apiRouter.POST("/message/action/", jwt.Auth(), controller.MessageAction)
}
